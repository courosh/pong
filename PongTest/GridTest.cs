﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pong.Grid;
using Pong.Output;
using Pong.Vector;

namespace PongTest
{
    [TestClass]
    public class GridTest
    {
        [TestMethod]
        public void cellTest()
        {
            Cell cell = new Cell(StateFactory.getInstance(StateType.NormalState),OutputFactory.getInstance(OutputType.Empty));
            Assert.AreEqual(cell.state,StateFactory.getInstance(StateType.NormalState));
        }

        [TestMethod]
        public void nextCo()
        {
            Coordinate dimension = new Coordinate(10, 10);

            BackgroundGrid background = new BackgroundGrid(dimension);
            background.initialBackground(StateType.NormalState, OutputType.Empty);
            MainGrid grid = new MainGrid(dimension, background);

            Coordinate coordinate = new Coordinate(5, 5);
            Coordinate vel = new Coordinate(0, 1);
            VectorObj obj = new VectorObj(coordinate,ref vel,1,OutputType.Ball,StateType.NormalState,grid);

            for (int i = 0; i < 5; i++)
            {
                coordinate = obj.getNextCoordinate(coordinate);
            }
            Assert.IsTrue((coordinate.y == 10) &&( coordinate.x== 5));

        }
        [TestMethod]
        public void backgroundtest()
        {
            Coordinate dimension = new Coordinate(10, 10);

            BackgroundGrid background = new BackgroundGrid(dimension);
            background.initialBackground(StateType.NormalState,OutputType.Empty);

            Assert.IsNotNull(background.grid);

            background.drawSquare(new Coordinate(0, 9), new Coordinate(0, 0), StateType.ReflectState, OutputType.Wall);

            Assert.IsInstanceOfType(background.getXY(new Coordinate(0,0)).state, typeof(ReflectState));


            MainGrid grid = new MainGrid(dimension,background);
            OutputGrid output = new OutputGrid(dimension);
            grid.subscribe(output);
            grid.setBackground();

            Coordinate co = new Coordinate(0, 0);
            Assert.IsTrue(grid.getXY(co).state.Equals( grid.backgroundGrid.getXY(co).state));
            Assert.IsTrue(output.getXY(co).Equals(grid.getXY(co).output));
        }
    }

}
