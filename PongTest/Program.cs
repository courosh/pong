﻿using Pong.Grid;
using Pong.Output;
using Pong.Vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pong
{
    class Program
    {
        static void Main(string[] args)
        {
            Coordinate dimension = new Coordinate(50, 50);

            BackgroundGrid background = new BackgroundGrid(dimension);
            background.initialBackground(StateType.NormalState, OutputType.Empty);
            background.drawSquare(new Coordinate(0, 49), new Coordinate(0, 0), StateType.VerticalReflectState, OutputType.Wall);
            background.drawSquare(new Coordinate(49,49), new Coordinate(49, 0), StateType.VerticalReflectState, OutputType.Wall);

            background.drawSquare(new Coordinate(0,0), new Coordinate(49, 0), StateType.ScoreState, OutputType.Empty);
            background.drawSquare(new Coordinate(0, 49), new Coordinate(49, 49), StateType.ScoreState, OutputType.Empty);

            MainGrid grid = new MainGrid(dimension, background);
            OutputGrid output = new OutputGrid(dimension);
            grid.subscribe(output);
            grid.setBackground();

            CMDOutput cmd = new CMDOutput(output);
            cmd.refreshRate = 100;

            Coordinate vel = new Coordinate(1,1);
            Coordinate vel3 = new Coordinate(1, 1);

            Coordinate start =new Coordinate(20,22);
            VectorObj ball = new VectorObj(start, ref vel, 1, OutputType.Ball, StateType.NormalState,grid);

            Coordinate start2 = new Coordinate(3, 22);
            VectorObj ball2 = new VectorObj(start2,ref  vel3, 10, OutputType.Ball, StateType.NormalState, grid);

            Coordinate vel2 = new Coordinate(1, 0);

            Tuple<Coordinate, Coordinate> batCoor = new Tuple<Coordinate, Coordinate>(new Coordinate(5, 5), new Coordinate(15, 5));
            VectorCollection bat = new VectorCollection();
            bat.boundaries=new Tuple<Coordinate, Coordinate>(new Coordinate(0, 49), new Coordinate(49, 0));
            bat.createSquareVectorObj(batCoor, ref vel2, 1, OutputType.Wall, StateType.HorizontolReflectState, grid);


            VectorOrchestra col = new VectorOrchestra();
            col.vObj.Add(ball);
            col.vObj.Add(ball2);
            col.vObj.Add(bat);

            Thread thread1 = new Thread(new ThreadStart(cmd.feedScreen));
            Thread thread2= new Thread(new ThreadStart(col.moveObjCycle));

            thread1.Start();
            thread2.Start();



        }


    }
}
