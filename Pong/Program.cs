﻿using Pong.Grid;
using Pong.Input;
using Pong.Output;
using Pong.Vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pong
{
    class Program
    {
        static void Main(string[] args)
        {
            Coordinate dimension = new Coordinate(50, 50);

            BackgroundGrid background = new BackgroundGrid(dimension);
            background.initialBackground(StateType.NormalState, OutputType.Empty);
            background.drawSquare(new Coordinate(0, 49), new Coordinate(0, 0), StateType.VerticalReflectState, OutputType.Wall);
            background.drawSquare(new Coordinate(49,49), new Coordinate(49, 0), StateType.VerticalReflectState, OutputType.Wall);

            background.drawSquare(new Coordinate(0,0), new Coordinate(49, 0), StateType.ScoreState, OutputType.Empty);
            background.drawSquare(new Coordinate(0, 49), new Coordinate(49, 49), StateType.ScoreState, OutputType.Empty);



            MainGrid grid = new MainGrid(dimension, background);
            OutputGrid output = new OutputGrid(dimension);
            grid.subscribe(output);
            grid.setBackground();

            CMDOutput cmd = new CMDOutput(output);
            cmd.refreshRate = 200;

            //ball
            Coordinate vel = new Coordinate(1,1);
            Coordinate start = new Coordinate(3, 22);
            VectorObj ball = new VectorObj(start, ref vel, 15, OutputType.Ball, StateType.HorizontolReflectState, grid);


            int batSpeed = 50;
            Tuple<Coordinate,Coordinate> boundaries = new Tuple<Coordinate, Coordinate>(new Coordinate(1, 48), new Coordinate(48, 1));


            //bat 1
            Coordinate batVel1 = new Coordinate(1, 0);
            Tuple<Coordinate, Coordinate> batCoor = new Tuple<Coordinate, Coordinate>(new Coordinate(5, 5), new Coordinate(15, 5));
            VectorCollection bat = new VectorCollection();
            bat.boundaries = boundaries;
            bat.createSquareVectorObj(batCoor, ref batVel1, batSpeed, OutputType.Wall, StateType.HorizontolReflectState, grid);

            //bat 2
            Coordinate batVel2 = new Coordinate(1, 0);
            Tuple<Coordinate, Coordinate> batCoor2 = new Tuple<Coordinate, Coordinate>(new Coordinate(5, 43), new Coordinate(15, 43));
            VectorCollection bat2 = new VectorCollection();
            bat2.boundaries = boundaries;
            bat2.createSquareVectorObj(batCoor2, ref batVel2, batSpeed, OutputType.Wall, StateType.HorizontolReflectState, grid);

            //controllers
            IVectorController player1 = new VectorController(bat);
            IVectorController player2 = new VectorController(bat2);



            //key bindings
            CMDInput input = new CMDInput();
            input.keyBinding[ConsoleKey.Q] = new keyPressedDelegate(player1.moveLeft);
            input.keyBinding[ConsoleKey.W] = new keyPressedDelegate(player1.moveRight);
            input.keyBinding[ConsoleKey.O] = new keyPressedDelegate(player2.moveLeft);
            input.keyBinding[ConsoleKey.P] = new keyPressedDelegate(player2.moveRight);

            VectorOrchestra col = new VectorOrchestra();
            col.vObj.Add(ball);
            col.vObj.Add(bat);
            col.vObj.Add(bat2);


            Thread thread1 = new Thread(new ThreadStart(cmd.feedScreen));
            Thread thread2= new Thread(new ThreadStart(col.moveObjCycle));
            Thread thread3 = new Thread(new ThreadStart(input.readKeys));


            thread1.Start();
            thread2.Start();
            thread3.Start();




        }


    }
}
