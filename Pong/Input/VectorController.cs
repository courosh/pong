﻿using Pong.Vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Input
{
    class VectorController : IVectorController
    {

        private VectorCollection collection { get; set; }

        public VectorController(VectorCollection collection)
        {
            this.collection = collection;
        }

        public void moveRight()
        {
            collection.velocity.x = 1;
        }

        public void moveLeft()
        {
            collection.velocity.x = -1;
        }
    }
}
