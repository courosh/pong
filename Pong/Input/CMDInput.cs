﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Input
{
    class CMDInput
    {
        public Dictionary<ConsoleKey, keyPressedDelegate> keyBinding = new Dictionary<ConsoleKey, keyPressedDelegate>();
        public void readKeys()
        {
            while (true)
            {
               ConsoleKeyInfo key= Console.ReadKey();
               callDelegate(key.Key);
            }

        }

        private void callDelegate(ConsoleKey key)
        {
           if(keyBinding.ContainsKey(key))
            {
                keyBinding[key].Invoke();
            }
        }


    }
}
