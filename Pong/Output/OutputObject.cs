﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Output
{
    public interface OutputObject
    {
        char charValue { get; set; }
    }
}
