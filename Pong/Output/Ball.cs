﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Output
{
    class Ball:OutputObject
    {
        public char charValue { get; set; } = 'O';
    }
}
