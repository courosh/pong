﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Output
{
    public class Score
    {
        public static int p1 { get; set; } = 0;
        public static int p2 { get; set; } = 0;

        public static  string scoreString()
        {
            return String.Format("Player 1:{0}" + Environment.NewLine + "Player 2:{1}", p1, p2);
        }
    }
}
