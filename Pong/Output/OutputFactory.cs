﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Output
{
    public class OutputFactory
    {

            private static OutputObject _wallInstance;
            private static OutputObject wallInstance
            {
                get
                {
                    if ((_wallInstance != null))
                    {
                        return _wallInstance;
                    }
                    else
                    {
                        return _wallInstance = new Wall();

                    }
                }
            }

            private static OutputObject _emptyInstance;
            private static OutputObject emptyInstance
            {
                get
                {
                    if ((_emptyInstance != null))
                    {
                        return _emptyInstance;
                    }
                    else
                    {
                        return _emptyInstance = new Empty();

                    }
                }
            }

        private static OutputObject _ballInstance;
        private static OutputObject ballInstance
        {
            get
            {
                if ((_ballInstance != null))
                {
                    return _ballInstance;
                }
                else
                {
                    return _ballInstance = new Ball();

                }
            }
        }



        public static OutputObject getInstance(OutputType state)
        {
            if (state == OutputType.Wall)
            {
                return wallInstance;
            }
            else if (state == OutputType.Empty)
            {
                return emptyInstance;
            }
            else if (state == OutputType.Ball)
            {
                return ballInstance;
            }
            else
            {
                throw new Exception("Unknown OutputObject");
            }
        }


        
    }

}
