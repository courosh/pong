﻿using NLog;
using Pong.Grid;
using Pong.Grid.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Output
{
    public class OutputGrid:BaseGrid<OutputObject>,IObserver
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();


        public OutputGrid(Coordinate dimension):base(dimension)
        {
        }

        
        public void update(Coordinate coordinate,Cell cell)
        {
            setXY(coordinate, cell.output);
        }

    }
}
