﻿using Pong.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pong.Output
{
    public class CMDOutput
    {
        public OutputGrid output;
        public int refreshRate { get; set; }=100;

        public CMDOutput(OutputGrid output)
        {
            this.output = output;
        }

        private String printGridOutput()
        {
            String grid="";
            for(int j=output.dimension.y-1;j>=0;j--)
            {
                for(int i=0; i<output.dimension.x;i++)
                {
                    grid+=output.getXY(new Coordinate(i,j)).charValue;
                }
                grid += Environment.NewLine;
            }
            grid += Score.scoreString();

            return grid;
        }

        public void feedScreen()
        {
            while(true)
            {
                Console.Clear();
                Console.Write(printGridOutput());
                Thread.Sleep(refreshRate);
            }
        }
    }
}
