﻿using Pong.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Vector
{
    public interface IVectorObj
    {
        Coordinate coordinate { get; set;}
        Coordinate velocity { get; set; }
        int speed { get; set; }
        Coordinate nextCoordinate { get; set; }

        
        Coordinate getNextCoordinate(Coordinate coordinate);
    }
}
