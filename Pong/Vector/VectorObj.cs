﻿using NLog;
using Pong.Grid;
using Pong.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pong.Vector
{
    public class VectorObj:IVectorObj,IVectorCycle
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Coordinate coordinate { get; set; }
        public Coordinate velocity { get; set; }
        public int speed { get; set; }

        private Timer timer { get; set; }
        private Coordinate _nextCoordinate;
        private Coordinate prevCoordinate{ get; set; }
        private Cell cell { get; set; }
        private MainGrid grid { get; set; }

        public Coordinate nextCoordinate
        {
            get
            {
                return _nextCoordinate;
            }
            set
            {
                _nextCoordinate = value;
                logger.Debug("Obj {0} coordinate {1} ",this.GetHashCode(), _nextCoordinate.ToString());
                triggerNexCell();
            }
        }

        public VectorObj(Coordinate coordinate,ref Coordinate velocity, int speed, OutputType output, StateType state, MainGrid grid)
        {
            this.coordinate = coordinate;
            this.velocity = velocity;
            this.speed = speed;
            this.cell  =new Cell(StateFactory.getInstance(state), OutputFactory.getInstance(output));
            this.grid = grid;
            pushCellToGrid();
        }

        public void startTimerDelegate()
        {
           TimerCallback callback = this.unitMoveDelegate;
           timer= new Timer(callback,null,0,1000/speed);
        }
         
        private void unitMoveDelegate(object obj)
        {
            this.unitMove();
        }

        public void unitMove()
        {
            prevCoordinate = coordinate;
            nextCoordinate = getNextCoordinate(this.coordinate);
            coordinate = nextCoordinate;
            restoreBackgroundCell(prevCoordinate);
            pushCellToGrid();
        }

        public Coordinate getNextCoordinate(Coordinate coordinate)
        {
            Coordinate next=new Coordinate(0,0);
            next.x = coordinate.x + (velocity.x);
            next.y = coordinate.y + (velocity.y);
            return next;
        }

        private void pushCellToGrid()
        {
            grid.setXY(coordinate, cell);

        }

        private void restoreBackgroundCell(Coordinate prevCoordinate)
        {
            if(grid.getXY(prevCoordinate)==this.cell)
            {
                grid.restoreBackgroundCell(prevCoordinate);
            }
        }

        private void triggerNexCell()
        {
            grid.getXY(nextCoordinate).state.onTrigger(this,grid);
        }



    }
}
