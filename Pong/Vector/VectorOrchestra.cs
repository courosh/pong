﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pong.Vector
{
    public class VectorOrchestra
    {
        public List<IVectorCycle> vObj = new List<IVectorCycle>();
        public void moveObjCycle()
        {

            foreach (IVectorCycle obj in vObj)
            {
                obj.startTimerDelegate();
            }

        }
    }
}
