﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Vector
{
    public enum Direction
    {
        N,
        S,
        W,
        E,
        NW,
        NE,
        SW,
        SE
    }
}
