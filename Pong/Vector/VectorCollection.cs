﻿using NLog;
using Pong.Grid;
using Pong.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pong.Vector
{
    public class VectorCollection:IVectorCycle
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Timer timer { get; set; }
        private int speed { get; set; }
        public Coordinate velocity { get; set; }
     
        public Tuple<Coordinate,Coordinate> boundaries { get; set; }
        public List<VectorObj> vectors = new List<VectorObj>();

        public void createSquareVectorObj(Tuple<Coordinate,Coordinate> coordinate,ref Coordinate velocity,int speed,OutputType output,StateType state,MainGrid grid)
        {
            this.speed = speed;
            this.velocity = velocity;
            for(int i=coordinate.Item1.x;i<=coordinate.Item2.x;i++)
            {
                for (int j = coordinate.Item2.y; j <= coordinate.Item1.y; j++)
                {
                    vectors.Add(new VectorObj(new Coordinate(i, j), ref velocity, speed, output, state,grid));
                }
            }
        }

        public void startTimerDelegate()
        {
            TimerCallback callback = collectionCycle;
            timer = new Timer(callback, null, 0, 1000 / speed);   
        }

        private void collectionCycle(object obj)
        {
            this.checkBoundaries(vectors.First());
            this.checkBoundaries(vectors.Last());
 
            foreach (VectorObj vec in vectors)
            {    
                vec.unitMove();            
            }     
        }

        private void checkBoundaries(VectorObj vec)
        {
            Coordinate next = vec.getNextCoordinate(vec.coordinate);
            if(next.x<boundaries.Item1.x|| next.x > boundaries.Item2.x||next.y<boundaries.Item2.y||next.y>boundaries.Item1.y)
            {
                vec.velocity.x = 0;
                vec.velocity.y = 0;
            }
        }
    }
}
