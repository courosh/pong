﻿using Pong.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Vector
{
    class Movement
    {

        public static readonly Dictionary<Direction,Coordinate> directionValue = new Dictionary<Direction,Coordinate>()
        {
            {Direction.N, new Coordinate(0,1) },
            {Direction.S, new Coordinate(0,-1) },
            {Direction.W, new Coordinate(-1,0) },
            {Direction.E, new Coordinate(1,0) },
            {Direction.NW, new Coordinate(-1,1) },
            {Direction.NE, new Coordinate(1,1) },
            {Direction.SW, new Coordinate(-1,-1) },
            {Direction.SE, new Coordinate(1,-1) }

        };


        


    }
}
