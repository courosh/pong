﻿using NLog;
using Pong.Grid.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public abstract class BaseGrid<T>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Coordinate dimension { get; set; }
        public T[,] grid { get; set; }

        public BaseGrid(Coordinate dimension)
        {   
            this.dimension = dimension;
            grid = new T[dimension.x, dimension.y];
            logger.Info("Created grid {0} by {1}", dimension.x, dimension.y);
        }

        public virtual void setXY(Coordinate coordinate , T obj)
        {
            grid[coordinate.x, coordinate.y] = obj;
        }

        public T getXY(Coordinate coordinate)
        {
            try
            {
                return grid[coordinate.x, coordinate.y];

            }
            catch (IndexOutOfRangeException e)
            {
                logger.Error(e,"Coordinate: {0}{1} ", coordinate.x, coordinate.y);
                throw e;
            }
        }

        public Coordinate getCenterCoordinate()
        {
            int x  = (int)Math.Round(this.dimension.x / 2.0);
            int y = (int)Math.Round(this.dimension.y / 2.0);

            return new Coordinate(x,y);
        }

        public IEnumerable<Coordinate> loopGrid()
        {
           return  this.loopGrid(new Coordinate(0,dimension.y-1),new Coordinate(dimension.x-1, 0));
        }

        public  IEnumerable<Coordinate> loopGrid(Coordinate topLeft, Coordinate bottomRight)
        {

            for (int i=topLeft.x;i<=bottomRight.x ; i++)
            {
                for (int j = bottomRight.y; j <=topLeft.y; j++)
                {
                    yield return new Coordinate(i,j);
                }
            }
        }

    }
}
