﻿using Pong.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public class Cell
    {
      
        public IState state { get; private set; }
        public OutputObject output { get; private set; }

        public Cell(IState state, OutputObject output)
        {
            this.state=state;
            this.output = output;
        }



    }
}
    