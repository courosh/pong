﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public enum StateType
    {
        NormalState,
        ReflectState,
        VerticalReflectState,
        HorizontolReflectState,
        DiagonalRefectState,
        ScoreState
    }
}
