﻿using NLog;
using Pong.Grid;
using Pong.Grid.Interface;
using Pong.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public class MainGrid:BaseGrid<Cell>,ISubject
    {   

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<IObserver> observers = new List<IObserver>();
        public BackgroundGrid backgroundGrid;

        public MainGrid(Coordinate dimension, BackgroundGrid backgroundGrid):base(dimension)
        {
            this.backgroundGrid = backgroundGrid;
        }
        
        public void setBackground()
        {
            foreach(Coordinate coordinate in loopGrid())
            {
                setXY(coordinate,backgroundGrid.getXY(coordinate));
            }
        }

    
        public override void setXY(Coordinate coordinate ,Cell cell)
        {
            notify(coordinate, cell);
            base.setXY(coordinate, cell);
        }

        public void restoreBackgroundCell(Coordinate coordinate)
        {
            setXY(coordinate, backgroundGrid.getXY(coordinate));
        }

        public void subscribe(IObserver observer)
        {
            observers.Add(observer);
        }

        public void unsubscribe(IObserver observer)
        {
            observers.Remove(observer);
        }   


        public void notify(Coordinate coordinate, Cell cell)
        {
            foreach(IObserver observer in observers)
            {
                observer.update(coordinate, cell);
            }
        }

    }
}
