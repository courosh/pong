﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid.Interface
{
    public interface ISubject
    {
        void subscribe(IObserver observer);
        void unsubscribe(IObserver observer);
     }
}
