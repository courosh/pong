﻿using Pong.Vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public interface IState
    {
          //void beforeTrigger(IVectorObj obj);  
          void onTrigger(IVectorObj obj, BaseGrid<Cell> grid);
         // void afterTrigger(IVectorObj obj);
        }
}
