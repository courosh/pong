﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public class StateFactory
    {
        private static IState _normalStateIns;
        private static IState normalStateIns
        {
            get
            {
                if((_normalStateIns!=null))
                {
                    return _normalStateIns;
                }
                else
                {
                    return _normalStateIns = new NormalState();
                    
                }
            }
        }

        private static IState _reflectStateIns;
        private static IState reflectStateIns
        {
            get
            {
                if ((_reflectStateIns != null))
                {
                    return _reflectStateIns;
                }
                else
                {
                    return _reflectStateIns = new ReflectState();

                }
            }
        }



        private static IState _hReflectStateIns;
        private static IState hReflectStateIns
        {
            get
            {
                if ((_hReflectStateIns != null))
                {
                    return _hReflectStateIns;
                }
                else
                {
                    return _hReflectStateIns = new HorizontolReflect();

                }
            }
        }

        private static IState _vReflectStateIns;
        private static IState vReflectStateIns
        {
            get
            {
                if ((_vReflectStateIns != null))
                {
                    return _vReflectStateIns;
                }
                else
                {
                    return _vReflectStateIns = new VerticalReflect();

                }
            }
        }

        private static IState _dReflectStateIns;
        private static IState dReflectStateIns
        {
            get
            {
                if ((_dReflectStateIns != null))
                {
                    return _dReflectStateIns;
                }
                else
                {
                    return _dReflectStateIns = new DiagonalReflect();

                }
            }
        }

        private static IState _scoreStateIns;
        private static IState scoreStateIns
        {
            get
            {
                if ((_scoreStateIns != null))
                {
                    return _scoreStateIns;
                }
                else
                {
                    return _scoreStateIns = new ScoreState();

                }
            }
        }


        public static IState getInstance(StateType state)
        {
            if(state==StateType.NormalState)
            {
                return normalStateIns;
            }
            else if (state == StateType.ReflectState)
            {
                return reflectStateIns;
            }
            else if (state == StateType.VerticalReflectState)
            {
                return vReflectStateIns;
            }
            else if (state == StateType.HorizontolReflectState)
            {
                return hReflectStateIns;
            }
            else if (state == StateType.DiagonalRefectState)
            {
                return dReflectStateIns;
            }
            else if (state == StateType.ScoreState)
            {
                return scoreStateIns;
            }
            else
            {
                throw new Exception("Unknown state");
            }
        }

    
    }
}
