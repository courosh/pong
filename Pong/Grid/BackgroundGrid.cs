﻿using NLog;
using Pong.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    public class BackgroundGrid:BaseGrid<Cell>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();


        public BackgroundGrid(Coordinate dimension):base(dimension)
        {
        }

        public void initialBackground(StateType initialState, OutputType initialOutput)
        {
            foreach (Coordinate coordinate in loopGrid())
            {
                Cell cell = new Cell(StateFactory.getInstance(initialState), OutputFactory.getInstance(initialOutput));
                setXY(coordinate, cell);
            }
        }

        public void drawSquare(Coordinate topLeft,Coordinate bottomRight, StateType state, OutputType output)
        {
            //TODO check input
            foreach (Coordinate coordinate in loopGrid(topLeft, bottomRight))
            {
                Cell cell = new Cell(StateFactory.getInstance(state), OutputFactory.getInstance(output));
                setXY(coordinate, cell);
            }

        }

        public void setCell(Coordinate coordinate , StateType state, OutputType output)
        {
            setXY(coordinate,new Cell(StateFactory.getInstance(state), OutputFactory.getInstance(output)));
        }

    
    }
}
