﻿using Pong.Output;
using Pong.Vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    class ScoreState:IState
    { 

        public void beforeTrigger(IVectorObj obj)
        {

        }

        public void onTrigger(IVectorObj obj, BaseGrid<Cell> grid)
        {
            if(obj.velocity.y>0)
            {
                Score.p1++;
            }
            else
            {
                Score.p2++;
            }
            obj.nextCoordinate = grid.getCenterCoordinate();

        }

        public void afterTrigger(IVectorObj obj)
        {

        }

    }
}
