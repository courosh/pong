﻿using Pong.Vector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong.Grid
{
    class VerticalReflect : IState
    {
        public void beforeTrigger(IVectorObj obj)
        {

        }

        public void onTrigger(IVectorObj obj,BaseGrid<Cell> grid)
        {
            obj.velocity.x *= -1;
        }

        public void afterTrigger(IVectorObj obj)
        {

        }
    }
}